#! /usr/bin/env perl

use Carp;
use strict;
use File::Copy;
use File::Path 'remove_tree';

my $file;
{
	open(my $IN,"<","react_dark") or croak("Couldn't open react_dark");
	local $/;
	$file = <$IN>;
	close $IN;
}

#my @pvar_strings = ("1.d-2","1.d-1","5.d-1","1.d0","2.d0","5.d0","1.d1","5.d1","1.d2"); 
my @pvar_strings = ("1.d0","5.d0","1.d1","5.d1","1.d2");
my @evar_strings = ("1.d0","5.d-1","1.d-1","1.d-2");
my @avar_strings = ("1.3704d0");
my $destination;

if ($file!~/(Dalpha\s =\s \d+\.\d*d\-?\d+\*alpha)/xms) {
  die "Dalpha not in form Dalpha = 1.d0*alpha";
} else {
  print "found match $1\n";
}
#@dark: de_mass = 1.d0*e_mass
if ($file!~/(de_mass\s =\s \d+\.\d*d\-?\d+\*e_mass)/xms) {
  die "de_mass not in form de_mass = 1.d0*e_mass";
} else {
  print "found match $1\n";
}
#@dark: dp_mass = 1.d0*p_mass
if ($file!~/(dp_mass\s =\s \d+\.\d*d\-?\d+\*p_mass)/xms) {
   die "dp_mass not in form dp_mass = 1.d0*p_mass";
} else {
   print "found match $1 \n";
}
   

foreach my $e_mass_string (@evar_strings) {
	
	print "found match $1 \n" if $file=~/(de_mass\s =\s \d+\.\d*d\-?\d+\*e_mass)/xms;
	
	$file =~ s/de\_mass\s =\s \d+\.\d*d\-?\d+\*e\_mass/de\_mass\ =\ $e_mass_string*e\_mass/xgsm;
	
	print("Setting de_mass string to $e_mass_string\n");
	
	open(my $OUT, '>', "react_dark") or die "Can't create react_dark: $!\n";
	print($OUT $file);
	close $OUT;
	
	
	foreach my $alpha_string (@avar_strings) {
		
		print "found match $1 \n" if $file=~/(Dalpha\s =\s \d+\.\d*d\-?\d+\*alpha)/xms;
		
		$file =~ s/Dalpha\s =\s \d+\.\d*d\-?\d+\*alpha/Dalpha\ =\ $alpha_string*alpha/xgsm;
		
		print("Setting Dalpha string to $alpha_string\n");
		
		open(my $OUT, '>', "react_dark") or die "Can't create react_dark: $!\n";
		print($OUT $file);
		close $OUT;
		
		foreach my $p_mass_string (@pvar_strings) {
			# Change the file
			
			print "found match $1 \n" if $file=~/(dp_mass\s =\s \d+\.\d*d\-?\d+\*p_mass)/xms;
			
			$file =~ s/dp\_mass\s =\s \d+\.\d*d\-?\d+\*p\_mass/dp\_mass\ =\ $p_mass_string*p\_mass/xgsm;
			
			print("Setting dp_mass string to $p_mass_string\n");
			
			open(my $OUT, '>', "react_dark") or die "Can't create react_dark: $!\n";
			print($OUT $file);
			close $OUT;
		
			system("./darkkrome","-b","-C");
			
			$destination = "build/data_pm${p_mass_string}_em${e_mass_string}_a${alpha_string}";
			print("Overwriting $destination\n") if -e $destination;
			remove_tree($destination);
			move("build/data",$destination);
			copy("react_dark","$destination/react_dark");
			
		}
	}
}
