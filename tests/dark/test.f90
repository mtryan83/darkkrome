!################################################################
!This is a simple one-zone collapse test following
! the chemical and thermal evolution of a primordial cloud.
!The dynamics is described by the Larson-Penston-type
! similar solution and includes cooling and heating processes.
!For additional details look also to the DarkKROME, Omukai 2000, 
! and KROME papers.
!################################################################
program test_darkkrome

    use krome_main
    use krome_user
    use krome_user_commons
    use krome_cooling
    use krome_constants
    use starting_densities
    implicit none
    
    interface
        subroutine my_dump_flux(n,T,nfile_in)
          real*8, intent(in) :: n(*)
          real*8, intent(in) :: T
          integer,optional, intent(in) :: nfile_in
        end subroutine my_dump_flux
        subroutine open_out_file(nfile_in,fileName)
          integer, intent(inout) :: nfile_in
          character*40, intent(in) :: fileName
          integer :: ios
        end subroutine open_out_file
    end interface
    
    integer,parameter::rstep = 750000
    integer::i,j,unit,ios,numargs,dynamicDensity,relAbunError=0
    integer::unit_d,unit_c,unit_h,unit_r
    real*8::dtH,deldd
    real*8::tff,dd,dd1
    real*8::x(krome_nmols),Tgas,dt
    real*8::ntot
    real*8::rho,rhocrit,rho_nddm,rhotot,rho_b
    real*8::Ttmp,h,time,Tvir,gamma,krome_gamma,tsFlag,mvircheck
    real*8::mjeans,tcool,mvir,R,tsound,tH,tlook,heat_array(krome_nheats)
    real*8::G,mp,kb,cs,Told,xe,xHe,xH2,Msun,nmax,mu
    real*8::omega_m,omega_b,omega_dm,omega_ddm,omega_mz,epsilon,d,delta_c,delta_t,Mpcincm,Myrinsec,zred

    character*40 arg
    character*40 fileName,datFileName

    ! Constants
    G=6.6743d-08  ! G_Newton in cgs
    mp=p_mass  ! Proton mass in cgs
    kb=boltzmann_erg  ! k_B in cgs
    h=0.6770  ! Reduced Hubble constant
    omega_m=0.3107
    omega_b=0.022447/h/h
    omega_dm=omega_m*(1-omega_b/omega_m) ! amount of dark matter
    Mpcincm = 3.085678d24  ! one Megaparsec in cm
    delta_c = 18.d0*pi**2 !high redshift, no contribution from lambda
    delta_t = 9.d0*pi**2/16.d0! density contrast at turnaround
    Myrinsec = 60*60*24*365*1d6 
    Msun = 1.989d33  ! solar mass in g
    nmax = 1d22  ! Maximum number density we evolve to
    tH = 3.09e17/h ! Inverse Hubble in seconds

    rhocrit=(3 / (8*pi*G)) * (h*1.d7/Mpcincm)**2  ! \rho_crit today in g/cm^3


    dynamicDensity=0


    ! Default redshift, epsilon, Tvir
    zred = 20d0
    epsilon = 1
    Tvir = 9d3
  
    !Read CMDLINE Arguments to determine initial temp, redshift, epsilon and 
    !whether density evolution is adiabatic only or can switch to isobaric
    numargs = iargc()
    if (numargs.GE.1) then
        call getarg(1,arg)
        read(arg,*)Tvir
        print *,"Running TEST with starting temp: ",Tvir
        if (numargs.GE.2) then
        call getarg(2,arg)
        read(arg,*)dynamicDensity
        if(dynamicDensity > 0) print *,"Dynamic density calculations used"
            if (numargs.GE.3) then
                call getarg(3,arg)
                read(arg,*)zred
                print *,"Running TEST at redshift: ",zred
                if (numargs.GE.4) then
                    call getarg(4,arg)
                    read(arg,*)epsilon
                    print *,"Running TEST at epsilon: ",epsilon
                    if (numargs.GE.5) then
                        print *,"Error: wrong number of arguments: ",numargs
                        stop
                    end if
                endif
            endif
        endif
    endif
    
    !INITIAL CONDITIONS
    omega_ddm=epsilon*omega_dm
    krome_redshift = zred    !redshift
    call krome_set_zredshift(zred)
    call krome_set_Tcmb(2.725d0)    ! This should not be multiplied by xi!
    call krome_set_Tfloor(2.725d0*xi)

    rhotot = omega_m * rhocrit*(1+krome_redshift)**3*delta_c !This is the total matter density

    rho_b = rhotot * omega_b/omega_m
    rho_nddm = rhotot * omega_dm/omega_m * (1-epsilon) ! Non-Dissipative Dark Matter density

    
    !initialize KROME (mandatory)
    call krome_init()
    
    
    ! Use James' data to initialize abundances to compute mu and gamma
    x(:) = get_n(1.d0,qe_mass*g_to_keV,qp_mass*g_to_keV/1.e6,Dalpha,xi,epsilon)
    mu = krome_get_mu(x) ! For ordinary matter this would be 1.22
    gamma = krome_get_gamma_x(x(:),Tgas)
    ntot = (omega_ddm/omega_m)*rhotot/(mp*mu)
    
    ! To match the collapseZ test, uncomment the following 
    !ntot = 0.1d0             !total density, cm^-3, matching collapseZ test
    !rho_b = 0d0             ! matching collapseZ test
    !rho_nddm = 0d0             ! matching collapseZ test
    !gamma=5.d0/3.d0       ! matching collapseZ test
    
    print *,"Running with mu=",mu
    print *,"Running with gamma=",gamma

    
    
    Tgas = (gamma-1d0) * Tvir
    
    !species default, cm-3
    x(:) = 1d-40

    ! set individual species
    ! This is setup in 5 sections. To run the specific set, simply uncomment
    ! that section and comment the others. 

    ! To match collapseZ initial abundances w/ SM
    !x(KROME_idx_HE)        = 1d-40*ntot  ! w/o He
    !x(KROME_idx_HE)        = 0.0775*ntot ! w/ He
    !x(KROME_idx_H2)        = 1.d-6*ntot  
    !x(KROME_idx_E)         = 1d-4*ntot
    !x(KROME_idx_Hj)        = 1d-4*ntot
    !x(KROME_idx_H)         = ntot-(x(KROME_idx_E)+x(KROME_idx_Hj)) 
    ! To match D'amico initial abundances using SM chemistry
    !x(KROME_idx_H2)        = 1.d-10*ntot 
    !x(KROME_idx_E)         = 1d-8*ntot
    !x(KROME_idx_Hj)        = 1d-8*ntot
    !x(KROME_idx_H)         = ntot-(x(KROME_idx_E)+x(KROME_idx_Hj)) 
    ! To match collapseZ initial abundances using DM chemistry
    !x(KROME_idx_QH2)        = 1.d-6*ntot 
    !x(KROME_idx_QE)         = 1d-4*ntot
    !x(KROME_idx_QHj)        = 1d-4*ntot
    !x(KROME_idx_QH)         = ntot-(x(KROME_idx_QE)+x(KROME_idx_QHj)) 
    ! To match D'amico initial abundances using DM chemistry
    !x(KROME_idx_QH2)        = 1.d-10*ntot 
    !x(KROME_idx_QE)         = 1d-8*ntot
    !x(KROME_idx_QHj)        = 1d-8*ntot
    !x(KROME_idx_QH)         = ntot-(x(KROME_idx_QE)+x(KROME_idx_QHj)) 
    ! To generate initial abundances using 2110.11964 work
    x(:) = get_n(ntot,qe_mass*g_to_keV,qp_mass*g_to_keV/1.e6,Dalpha,xi,epsilon)
    
    print *,"QH2:",x(KROME_idx_QH2)/ntot,"QE:",x(KROME_idx_QE)/ntot,"QH+:",x(KROME_idx_QHj)/ntot, &
       "QH-:",x(KROME_idx_QHk)/ntot,"QH2+:",x(KROME_idx_QH2j)/ntot,"QH:",x(KROME_idx_QH)/ntot

    !set initial density
    dd = ntot

    ! VIRIAL MASS (in grams)
    ! Note: This is assuming a spherical, uniform density profile and
    ! gamma = 5/3
    mvir = (4 * pi * rhotot / 3.d0)**(-0.5) * (5 * kb * Tvir / (G*mp*krome_get_mu(x)))**1.5
    print *,"mu: ",krome_get_mu(x)
    mvircheck = mvir * omega_ddm/omega_m

    !open file to write explore data
    open(newunit=unit,file="explore.dat",status="replace")
    !open output files
    write(fileName,"(a,i0,a,i0,a,f5.3,a)")'run_t',int(Tvir,8),'_z',int(zred,8),'_e',epsilon,'.dat'
    datFileName = fileName
    print *,"Saving data file to ",fileName
    call open_out_file(unit_d,fileName)
    write(fileName,"(a,i0,a,i0,a,f5.3,a)")'run_t',int(Tvir,8),'_z',int(zred,8),'_e',epsilon,'.cool'
    print *,"Saving cool file to ",fileName
    call open_out_file(unit_c,fileName)
    write(fileName,"(a,i0,a,i0,a,f5.3,a)")'run_t',int(Tvir,8),'_z',int(zred,8),'_e',epsilon,'.heat'
    print *,"Saving heat file to ",fileName
    call open_out_file(unit_h,fileName)
    write(fileName,"(a,i0,a,i0,a,f5.3,a)")'run_t',int(Tvir,8),'_z',int(zred,8),'_e',epsilon,'.react'
    print *,"Saving react file to ",fileName
    call open_out_file(unit_r,fileName)

    print *,"solving..."
    print '(a5,2a11)',"step","n(cm-3)","Tgas(K)"

    !output header
    write(unit_d,*) "#ntot Tgas "//trim(krome_get_names_header())," tff tsound tc Mjeans pdv time"

    call dump_thermo_header(unit_c,unit_h)

    Told = Tgas

    time = 2.05712d19/(100*h*sqrt(omega_m)*(1+zred)**(1.5))

    !loop on density steps
    do i = 1,rstep

        !store old density
        dd1 = dd

        !free-fall time, s
        ! can't use krome_get_free_fall_time(x(:)) since it only accounts for 
        ! the matter being chemically evolved (i.e. it doesn't include rho_nddm)

        rho = krome_get_rho(x(:))
        tff = sqrt(3*pi/32/G/(rho+rho_nddm+rho_b))
        
        gamma = krome_get_gamma_x(x(:),Tgas)

        !!!!! Originally from private communication with Guido D'Amico
        !Cooling time
        Ttmp=Tgas
        cs=sqrt(gamma*kb*Tgas / (mp * krome_get_mu(x)))
        R=((mvir*omega_ddm/omega_m)/(4*pi*rho/3))**(1./3.)  ! Radius
        tsound = R/cs
        tcool = 3./2*kb*Tgas*sum(x)/cooling(x,Tgas)
        if(tcool<0) tcool=1.d38
        mjeans=(pi/6.) * rho * cs**3 * (pi/(G*(rho+rho_nddm+rho_b)))**1.5

        tsflag = mjeans * (3.d0/8.d0)**1.5;

        if (( dynamicDensity>0 ).and.( mvircheck<=tsflag )) then
            ! "Evolve" density isobarically (rho = T_0 rho_0 / T)
            user_tff = tsound
            dtH = 0.01d0 * user_tff
            dd = dd*Told/Tgas ! rhodm/mp*omega_ddm/omega_m * Tvir/Tgas
            call krome_set_pdv(0)
        else
            user_tff = tff         !store user tff
            dtH = 0.01d0 * tff     !define time-step
            deldd = (dd/tff) * dtH !density increase
            dd = dd + deldd        !update density
            call krome_set_pdv(1)
        end if

        !rescale density
        x(:) = x(:)*dd/dd1

        !set time-step
        dt = dtH

        !break when max density reached
        if(dd.gt.1.d15) exit
        
        
        !print initial conditions
        if (i==1) then
            call krome_dump_cooling(x(:),Tgas,unit_c)
            call krome_dump_heating(x(:),Tgas,unit_h)
            call my_dump_flux(x(:),Tgas,unit_r)
            write(unit_d,'(99E17.8e3)') dd1,Tgas,x(:)/dd1,tff,tsound,tcool,mjeans,krome_get_pdv()*1.d0,time
            print '(I7,99E11.3)',i,dd1,Tgas,zred,krome_redshift
        end if

        !solve the chemistry
        call krome(x(:),Tgas,dt)
        if (Tgas.lt.0) then
            print *,"Error: negative temperature. Stopping"
            exit
        end if
        
        !avoid 0 species
        do j=1,krome_nmols
            x(j) = max(x(j),1.d-100)
            if ((j.ne.KROME_idx_QG).and.((x(j)/dd).gt.1.5d0)) then
                print *,"Error: species",j,"has relative abundance >1. Stopping"
                relAbunError = 1
                exit
            end if
        end do
        if (relAbunError.gt.0) then
            exit
        end if

        time = time+dt
        
        ! Compute new redshift
        zred = 7.50769d12/(time**2 * (100*h)**2 * omega_m)**(1.d0/3.d0) - 1.d0
        ! If new redshift differs from old redshift by more than 5%, update
        ! We do this because some redshift dependent calculations are slow and 
        ! the results cached. So update those as infrequently as possible.
        krome_redshift = krome_get_zredshift()
        if((zred.GE.0.d0).AND.((2*abs(zred-krome_redshift)/(zred+krome_redshift)).GE.0.05)) then
            call krome_set_zredshift(zred)
        end if

        Told = Tgas


        call krome_dump_cooling(x(:),Tgas,unit_c)
        call krome_dump_heating(x(:),Tgas,unit_h)
        call my_dump_flux(x(:),Tgas,unit_r)

        !print some output
        write(unit_d,'(99E17.8e3)') dd,Tgas,x(:)/dd,tff,tsound,tcool,mjeans,krome_get_pdv()*1.d0,time
        if(mod(i,100)==0) then
            print '(I7,99E11.3)',i,dd,Tgas,zred,krome_redshift
            call krome_print_best_flux(x(:),Tgas,5)
        end if

    end do

!close explore data file
close(unit)

!say goodbye
print *,"To plot type in gnuplot:"
print *,"gnuplot> datafile='",trim(datFileName),"'"
print *,"gnuplot> load 'plot.gps'"
print *,"That's all! have a nice day!"

end program test_darkkrome

!************************
!Better version of dump flux that does
!not put each reaction on its own line
!dump the fluxes to the file unit nfile
subroutine my_dump_flux(n,Tgas,nfile_in)
    use krome_commons
    use krome_user
    implicit none
    real*8, intent(in) :: n(nmols)
    real*8, intent(in) :: Tgas
    real*8::flux(nrea)
    integer,optional, intent(in) :: nfile_in
    integer::i,nfile
    nfile = 33
    if(present(nfile_in)) nfile = nfile_in
    flux(:) = krome_get_flux(n(:),Tgas)
    write(nfile,'(99E14.5e3)') Tgas,flux(:)

end subroutine my_dump_flux
subroutine open_out_file(nfile_in,fileName)
    implicit none
    integer, intent(inout) :: nfile_in
    character*40, intent(in) :: fileName
    integer :: ios
    open(newunit=nfile_in,file=fileName,status="replace",iostat=ios)
    if (ios /= 0) then
    	print *,"Error opening file ",fileName," iostat=",ios
    	stop 1
    end if
end subroutine open_out_file    

