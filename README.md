# This is the [DARKKROME](https://bitbucket.org/mtryan83/darkkrome) repository

DarkKROME is an extension/fork of the KROME package with a complex dark matter 
 model added.
 
DarkKROME is designed as a drop-in replacement for KROME, and as such, this 
repository is a fork of the KROME repository with appropriate
changes to several of the key program/source files to add in dark chemistry.

An example test program can be found in the tests/dark directory, and can 
be generated with `./darkkrome.py -test=DARK`, in the same manner as other KROME
tests. We have also included a bash script, `./darkkrome`, which provides a few
shortcuts to generating and running the test file in various cases. (The bash
script was used to generate the data for the figures in the DarkKROME paper, for
example.) The `react_dark_minimal` file provides the example chemical network 
used in the DarkKROME paper, while the `react_dark` file is used in the DARK test.


The DarkKROME paper can be found at [2110.11971](https://arxiv.org/abs/2110.11971),
based on the dark chemistry in [1705.10341](https://arxiv.org/abs/1705.10341)
and [2106.13245](https://arxiv.org/abs/2106.13245). 

The DARK test uses initial primordial abundances that depend on the dark 
parameter values m, M, alpha, xi, and epsilon. These are pulled from a dataset 
generated using the version of RecFAST++ from [2110.11964](https://arxiv.org/abs/2110.11964) 
and saved as an HDF5 file, `test/dark/abundances.h5`. We have included a Fortran 
module, `starting_densities` with function `get_n(ntot,m,M,alphaD,xi,epsilon)` that
provides these abundances. This module is found in 
`test/dark/starting_densities.f90`. Because of this, the default compiler for the
DARK test is `h5fc` and HDF5 needs to be installed in addition to the standard KROME
prerequisites.

To get started quickly, simply clone this repository: 
```
git clone https://bitbucket.org/mtryan83/darkkrome.git
```
Entering the resulting directory and runing the `darkkrome` bash script:
```
$ ./darkkrome
```
will run the DARK test at the default parameter set m=m_electron, M=m_proton, 
alpha=1/137, xi=0.01, epsilon=1, and initial temperature T=9000 K and 
redshift z=20. 
 
# The following pertains to the KROME package

KROME is a nice and friendly package to model chemistry and microphysics 
 for a wide range of astrophysical simulations. 
 Given a chemical network (in CSV-like format) it automatically 
 generates all the routines needed to solve the kinetic of the system, 
 modelled as system of coupled Ordinary Differential Equations. 
 It provides different options which make it unique and very flexible. 
 Any suggestions and comments are welcomed. KROME is an open-source 
 package, GNU-licensed, and any improvements provided by 
 the users is well accepted. See disclaimer below and GNU License 
 in gpl-3.0.txt.

--
## Get KROME
KROME is available on 

- [http://www.kromepackage.org](http://www.kromepackage.org)

 and

- [https://bitbucket.org/tgrassi/krome](https://bitbucket.org/tgrassi/krome)

You can quickly clone this repository by typing
```
git clone https://bitbucket.org/tgrassi/krome.git
```

---
## Get help

To get support or receive news about KROME please refer to our user mailing list: 

 - https://groups.google.com/forum/#!forum/kromeusers


More information on the wiki

 - https://bitbucket.org/tgrassi/krome/wiki/Home

Additional material can be found in the Computational Astrochemistry Schools website

 - http://kromepackage.org/bootcamp/

---
## Authors

Written and developed by Tommaso Grassi
```
 tgrassi@usm.lmu.de               
 USM/LMU, Munich
```

and Stefano Bovino
```
 stefanobovino@udec.cl            
 Departamento de Astronomia, Universidad de Concepcion, Chile
```

Contributors: J.Boulangier, T.Frostholm, D.Galli, F.A.Gianturco, T.Haugboelle,
  A.Lupi, J.Prieto, J.Ramsey, D.R.G.Schleicher, D.Seifried, E.Simoncini,
  E.Tognelli

---
## Speed test

If you want to test how fast is KROME on your machine go [here](https://bitbucket.org/tgrassi/krome_speed_test/overview).

---
## Disclaimer


KROME is provided "as it is", without any warranty. 
 The Authors assume no liability for any damages of any kind 
 (direct or indirect damages, contractual or non-contractual 
 damages, pecuniary or non-pecuniary damages), directly or 
 indirectly derived or arising from the correct or incorrect 
 usage of KROME, in any possible environment, or arising from 
 the impossibility to use, fully or partially, the software, 
 or any bug or malfunction.
 Such exclusion of liability expressly includes any damages 
 including the loss of data of any kind (including personal data)

---
## Trusted commit

Additional notes: this version of KROME is a developer version,
 while the stable version has been dropped. For this reason we
 warmly recommend to check the status of the release by using the
 test website
 http://kromepackage.org/test/

 More information on the "tested" version here
 https://bitbucket.org/tgrassi/krome/wiki/stable_version
