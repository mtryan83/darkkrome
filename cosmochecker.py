
import sys

from numpy import sqrt, pi, array, transpose, log10, round
from scipy.integrate.quadpack import quad
from scipy import interpolate


class cosmo():
    g_to_gev = 5.6096e23
    
    Dalpha = 7.2973525664e-3
    de_mass = 9.10938188e-28
    dp_mass = 100*1.67262158e-24
    dn_mass = 1.674920e-24
    de_mass_gev = 9.10938188e-28*g_to_gev
    dp_mass_gev = 100*1.67262158e-24*g_to_gev
    dn_mass_gev = 1.674920e-24*g_to_gev
    xi = 0.01
    
    
    delta_v = 178 # Buckley Difranzo 2018
    
    h = 0.6777 #Planck
    H0_base = 100*h # in (km/s)/Mpc
    H0 = H0_base*3.24078e-20 # 1/s
    G = 6.67259e-8 # cgs  #6.7073e-39 # GeV^-2
    
    k = 1.380658e-16 # erg/K
    c = 29979245800 # cm/s
    
    rho_0 = 3*H0**2/(8*pi*G)
    
    omega_dm = 0.1188/h**2 
    omega_b = 0.0223/h**2
    omega_l = 0.6911
    omega_g = 2.47e-5/h**2
    omega_m = omega_dm+omega_b
    
    
    z = 40
    delta_c = 18*pi**2 #High redshift, no contribution from lambda
    
    rhotot = omega_m * rho_0*(1+z)**3*delta_c #This is the total matter density
    ntot = (omega_dm/omega_m)*rhotot/dp_mass
    
    n_dp = 1e-8*ntot
    n_de = 1e-8*ntot
    n_HD = ntot-n_dp-n_de
    
    darkParams = {"de_mass":["9.10938188d-28","g"], #overridden dark parameters
        "dp_mass":["1.67262158d-24","g"],
        "dn_mass":["1.674920d-24","g"],
        "Dalpha":["7.2973525664d-3","Dark fine structure constant"],
        "xi":["1","Dark CMB temperature to Normal CMB temperature ratio"]} 
    
    def convertDarkParams(self,dark):
        self.Dalpha = dark["Dalpha"][1]
        self.dp_mass = dark["dp_mass"][1]*self.g_to_gev
        self.de_mass = dark["de_mass"][1]*self.g_to_gev
        self.dn_mass = dark["dn_mass"][1]*self.g_to_gev
        self.dp_mass_gev = self.dp_mass*self.g_to_gev
        self.de_mass_gev = self.de_mass*self.g_to_gev
        self.dn_mass_gev = self.dn_mass*self.g_to_gev
        self.xi = dark["xi"][1]
    
    def lambdaCDM_H(self):
        return lambda a: self.H0*sqrt(
            self.omega_l + 
            self.omega_m/a**3 + 
            self.omega_g/a**4 + 
            (1-self.omega_l-self.omega_b-self.omega_dm-self.omega_g)/a**2)
        
    def H(self,a):
        return self.H0*sqrt(
            self.omega_l + 
            self.omega_m/a**3 + 
            self.omega_g/a**4 + 
            (1-self.omega_l-self.omega_m-self.omega_g)/a**2)
        
    def rho_cr(self,z):
        return 3*self.H(1.0/(1+z))**2/(8*pi*self.G)
    
    def default_thermo(self): #TODO: use better cooling rate
        c = array([[-1124.65, 0, 253.937, -132.605],
                    [-56.8221, -238.561, 237.069, -115.048],
                    [230.406, -250.615, 202.481, -99.4981],
                    [173.251, -201.74, 170.496, -86.3528],
                    [118.359, -164.99, 144.566, -75.2448],
                    [96.7555, -139.884, 123.009, -65.8061],
                    [83.9811, -119.36, 104.679, -57.7736],
                    [72.0017, -101.546, 89.0589, -50.9391],
                    [60.947, -86.2727, 75.7788, -45.1243],
                    [51.7819, -73.3446, 64.4928, -40.1759],
                    [46.7001, -62.3606, 54.8974, -35.9642],
                    [45.3998, -52.4545, 46.7792, -32.3779],
                    [20.8286, -42.8242, 40.0423, -29.3164],
                    [-91.6695, -38.406, 34.2988, -26.6919],
                    [-194.313, -57.8511, 27.4927, -24.4911],
                    [110.062, -99.069, 16.3974, -22.9051],
                    [271.755, -75.7225, 4.03838, -22.2021],
                    [101.7, -18.0774, -2.59393, -22.1991],
                    [4.51757, 3.49532, -3.62499, -22.4369],
                    [-7.33613, 4.4536, -3.06294, -22.6741],
                    [-2.37637, 2.89745, -2.54317, -22.871],
                    [-0.621769, 2.39337, -2.16907, -23.0372],
                    [-0.836193, 2.26148, -1.83994, -23.1788],
                    [-1.1131, 2.0841, -1.53268, -23.2979],
                    [-1.17456, 1.84799, -1.25465, -23.3963],
                    [-1.12786, 1.59884, -1.01094, -23.4762],
                    [-1.028, 1.3596, -0.801755, -23.54],
                    [-0.89314, 1.14154, -0.624907, -23.5903],
                    [-0.737294, 0.952086, -0.476872, -23.6291],
                    [-0.571562, 0.795691, -0.353292, -23.6583],
                    [-0.416841, 0.67445, -0.249343, -23.6795],
                    [-0.340138, 0.586029, -0.160218, -23.6939],
                    [-0.350849, 0.513879, -0.0824467, -23.7024],
                    [0.254475, 0.439456, -0.0150392, -23.7058],
                    [3.1891, 0.493436, 0.0509229, -23.7046],
                    [4.33182, 1.16991, 0.168533, -23.6974],
                    [-25.4307, 2.08878, 0.398946, -23.6781],
                    [13.9555, -3.3056, 0.312908, -23.6485],
                    [4.55052, -0.345354, 0.0547595, -23.6379],
                    [-0.689532, 0.619908, 0.0741725, -23.6342],
                    [-0.840241, 0.473644, 0.151494, -23.6261],
                    [-0.345453, 0.295411, 0.205872, -23.6133],
                    [-0.178937, 0.222133, 0.242466, -23.5974],
                    [-0.146805, 0.184177, 0.271195, -23.5792],
                    [-0.122966, 0.153036, 0.295038, -23.5591],
                    [-0.0968562, 0.126953, 0.314836, -23.5376],
                    [-0.0754622, 0.106407, 0.331336, -23.5147],
                    [-0.0595805, 0.0904003, 0.345251, -23.4908],
                    [-0.0475988, 0.0777621, 0.357142, -23.4659],
                    [-0.0382713, 0.0676654, 0.367424, -23.4403],
                    [-0.0308788, 0.0595472, 0.376419, -23.414],
                    [-0.024956, 0.0529972, 0.384377, -23.3871],
                    [-0.0201719, 0.0477035, 0.391497, -23.3596],
                    [-0.0162899, 0.0434246, 0.397941, -23.3317],
                    [-0.0131415, 0.0399691, 0.403837, -23.3034],
                    [-0.010604, 0.0371815, 0.409292, -23.2746],
                    [-0.00858393, 0.0349322, 0.414391, -23.2455],
                    [-0.00700647, 0.0331114, 0.419202, -23.216],
                    [-0.00580874, 0.0316251, 0.42378, -23.1862],
                    [-0.00493554, 0.030393, 0.428165, -23.1561],
                    [-0.00433688, 0.029346, 0.432389, -23.1257],
                    [-0.00396671, 0.0284261, 0.436474, -23.095],
                    [-0.00378233, 0.0275847, 0.440434, -23.064],
                    [-0.00374425, 0.0267824, 0.444278, -23.0327],
                    [-0.00381631, 0.0259881, 0.448009, -23.0012],
                    [-0.00396583, 0.0251786, 0.451627, -22.9693],
                    [-0.00416383, 0.0243374, 0.455128, -22.9373],
                    [-0.0043852, 0.0234541, 0.458508, -22.905],
                    [-0.00460879, 0.0225239, 0.461759, -22.8725],
                    [-0.00481735, 0.0215463, 0.464875, -22.8397],
                    [-0.00499744, 0.0205245, 0.467849, -22.8067],
                    [-0.00513921, 0.0194644, 0.470677, -22.7735],
                    [-0.00523612, 0.0183743, 0.473352, -22.7402],
                    [-0.00528458, 0.0172636, 0.475872, -22.7066],
                    [-0.00528355, 0.0161426, 0.478234, -22.6729],
                    [-0.00523411, 0.0150218, 0.480438, -22.639],
                    [-0.00513908, 0.0139116, 0.482484, -22.6049],
                    [-0.00500257, 0.0128215, 0.484374, -22.5707],
                    [-0.00482961, 0.0117603, 0.486112, -22.5364],
                    [-0.00462581, 0.0107359, 0.487703, -22.502],
                    [-0.00439704, 0.00975463, 0.489151, -22.4675],
                    [-0.0041492, 0.00882192, 0.490465, -22.4328],
                    [-0.003888, 0.00794179, 0.49165, -22.3981],
                    [-0.00361879, 0.00711706, 0.492715, -22.3633],
                    [-0.00334644, 0.00634944, 0.493667, -22.3284],
                    [-0.0030753, 0.00563959, 0.494515, -22.2935],
                    [-0.00280911, 0.00498725, 0.495266, -22.2585],
                    [-0.00255101, 0.00439138, 0.495929, -22.2235],
                    [-0.00230354, 0.00385025, 0.496512, -22.1884],
                    [-0.00206862, 0.00336163, 0.497022, -22.1533],
                    [-0.0018476, 0.00292283, 0.497466, -22.1181],
                    [-0.00164173, 0.00253091, 0.497852, -22.0829],
                    [-0.00145321, 0.00218267, 0.498185, -22.0477],
                    [-0.00128286, 0.00187441, 0.498472, -22.0125],
                    [-0.00111288, 0.00160229, 0.498718, -21.9772],
                    [-0.00089485, 0.00136622, 0.498928, -21.9419],
                    [-0.000719966, 0.00117641, 0.499108, -21.9067],
                    [-0.00134835, 0.00102369, 0.499263, -21.8714],
                    [-0.00347761, 0.000737674, 0.499388, -21.836]])
        c = transpose(c)
        x = array([3, 3.07071, 3.14141, 3.21212, 3.28283, 3.35354, 3.42424, 3.49495, 3.56566, 3.63636, 3.70707, 3.77778, 3.84848, 3.91919, 3.9899, 4.06061, 4.13131, 4.20202, 4.27273, 4.34343, 4.41414, 4.48485, 4.55556, 4.62626, 4.69697, 4.76768, 4.83838, 4.90909, 4.9798, 5.05051, 5.12121, 5.19192, 5.26263, 5.33333, 5.40404, 5.47475, 5.54545, 5.61616, 5.68687, 5.75758, 5.82828, 5.89899, 5.9697, 6.0404, 6.11111, 6.18182, 6.25253, 6.32323, 6.39394, 6.46465, 6.53535, 6.60606, 6.67677, 6.74747, 6.81818, 6.88889, 6.9596, 7.0303, 7.10101, 7.17172, 7.24242, 7.31313, 7.38384, 7.45455, 7.52525, 7.59596, 7.66667, 7.73737, 7.80808, 7.87879, 7.94949, 8.0202, 8.09091, 8.16162, 8.23232, 8.30303, 8.37374, 8.44444, 8.51515, 8.58586, 8.65657, 8.72727, 8.79798, 8.86869, 8.93939, 9.0101, 9.08081, 9.15152, 9.22222, 9.29293, 9.36364, 9.43434, 9.50505, 9.57576, 9.64646, 9.71717, 9.78788, 9.85859, 9.92929, 10])
        pp = interpolate.PPoly(c,x)
        return pp
    
    #H = lambda a: 1
    thermo = lambda T: 1
    
    def __init__(self):
        self.thermo = self.default_thermo() 
        #self.H = self.lambdaCDM_H()
    
def checkDarkCosmoConstraints(cosmo,halo_mass=1e11,min_scale_factor=1e4):
        print "Checking constraints on dark parameters..."
        #Using constraints from Buckley and DiFranzo 2018
        error_check = False
        
        msol_to_g = 1.9891e33
        msol_to_gev = msol_to_g*cosmo.g_to_gev
        #dp_mass = self.darkParams["dp_mass"][1]*g_to_gev #GeV
        #de_mass = self.darkParams["de_mass"][1]*g_to_gev # GeV
        #dalpha = self.darkParams["Dalpha"][1]
        #xi = self.darkParams["xi"][1]
        dp_mass = cosmo.dp_mass
        dp_mass_gev = cosmo.dp_mass_gev
        de_mass = cosmo.de_mass
        de_mass_gev = cosmo.de_mass_gev
        dalpha = cosmo.Dalpha
        xi = cosmo.xi
        
        #self-scattering constraint
        if(dalpha**2/dp_mass_gev**3>5e-11):
            print "self-scattering constraint failed. alpha_D:",dalpha,"m_X:",dp_mass_gev," constraint:",dalpha**2/dp_mass_gev**3,"> 1e-11"
            error_check = True
        else:
            print "self-scattering constraint passed"
            
        #decoupling redshift
        beta = 1e16/3*dalpha**6*xi**2*1.0/dp_mass_gev
        
        if(beta>=1):
            zd = 8.5e8*(dalpha/.1)**2*de_mass_gev*(0.5/xi)
        else:
            zd = 3e4*(0.1/dalpha)*de_mass_gev*(dp_mass_gev)**(0.5)*(0.5/xi)**2
            
        if(zd<3500):
            print "WARNING: Dark decoupling occurs after matter-radiation equality: zd:",zd," < 3500"

        integr,err = quad(rdao_integrand,0.0,1.0,args=(cosmo,))
        r_dao = 1.0/sqrt(3)*integr
        rho_dm = cosmo.omega_dm*cosmo.rho_0
        M_min = 4.0*pi/3.0*rho_dm*r_dao**3
        print "rho_dm:",rho_dm,"r_dao:",r_dao*3.24078e-25
        
        halo_mass_g = halo_mass*msol_to_g
        
        if(halo_mass_g/min_scale_factor<M_min):
            print "halo mass: {:.0} less than minimum from DAO: {:.3}".format(halo_mass/min_scale_factor,M_min/msol_to_g)
            error_check = True
        else:
            print "halo mass above minimum: {0:.2} M_sol".format(M_min/msol_to_g)
            
        rho_v = cosmo.omega_dm*cosmo.rho_cr(cosmo.z)
        t_ff = sqrt(3.0*pi/(32*cosmo.G*cosmo.delta_v*rho_v))
        
        s_to_gy = 3.17098e-17
        
        n_dm = cosmo.ntot
        T_v = (4.0*pi/3.0*cosmo.delta_v*rho_v)**(1.0/3)*cosmo.G*dp_mass*halo_mass_g**(2.0/3)/cosmo.k
        
        Pi = 10**(cosmo.thermo(log10(T_v)))
        #print Pi,T_v,cosmo.thermo(log10(T_v)),Pi*(cosmo.n_dp+cosmo.n_HD)**2
        t_c = 3.0/2.0*n_dm*cosmo.k*T_v/(Pi*(cosmo.n_dp+cosmo.n_HD)**2)
        
        if(t_ff<t_c):
            print "Error: Freefall time:",t_ff*s_to_gy," Gy is less than critical time:",t_c*s_to_gy,"Gy"
            error_check = True   
        else:
            print "Freefall time (",t_ff*s_to_gy,"Gy ) > critical time (",t_c*s_to_gy," Gy )"  
            
        if(error_check):
            sys.exit()
        
        print "Dark Cosmology constraints passed"
            
def rdao_integrand(a,cosmo):
    rdi = cosmo.c/((a**2)*cosmo.H(a))/sqrt(1.0+3*a*cosmo.omega_dm/(4*cosmo.xi**4*cosmo.omega_g))
    return  rdi


# cos = cosmo()
# print "Cooling rate:",cos.thermo(1.09)
# checkDarkCosmoConstraints(cos)



